package com.linkwechat.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.linkwechat.domain.WeMomentsInteracte;

public interface WeMomentsInteracteMapper extends BaseMapper<WeMomentsInteracte> {
}
